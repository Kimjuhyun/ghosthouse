

function reserved(){
	var device = $("input[name=device]:checked").val();
  // var device = "android";
	var phone = $("input[name=phone]").val();
	var phoneCheck = /^[0-9]*$/;

	if(phone.length<=0) {
    popOpen('tel_none');
    return false;
	}else if(phone.length<10 || phone.length>11 || !phoneCheck.test(phone)) {
    popOpen('tel_error');
    return false;
	}else if(!$("input[name=personalCheck]").prop("checked")){
    popOpen('agree1');
    return false;
	} else {
    $.post('/book', {
      'phoneNumber': phone,
      'platform': device
    }, function(res) {
      if (res.result) {
        popOpen('regist_done');
        fbq('track', 'CompleteRegistration');
        flexPlatform();
      } else {
        if (res.code == '1' || res.code == '3') { //error
      		$("#confirmModal #content").html("잠시 후 다시 시도해주세요.");
      		$("#confirmModal").show();
        } else if (res.code == '2') {
          popOpen('already');
        }
      }
    });
  } 
}
function flexPlatform() {
  $.post('/flexplatform?datakey='+dataKey,{
    'datakey' : dataKey
  },function(res) {
      if(res.return_code === "0000") {
        console.log("flexplatform 성공");
      }
  });
}
