
$(window).on('resize load scroll', function(){
    popResize();
    onScroll();
    $(document).on("scroll", onScroll);
});
$(document).ready(function(){
	$("#main").addClass("motion").delay(500).queue(function(next){
	    $(".elebox").addClass("start");
	    next();
	});
	$(".grand_stage > span").each(function(){
		var _this =$(this);
		var _attr =$(this).attr("data");
		var _imgbox =$(this).find("em");
		_this.hover(function(){
			$(".grand_stage  span  em").css("display" ,"none")
			_imgbox.css("display" ,"block").find("img").attr("src" , _attr)
		},
		function(){
			_imgbox.css("display" ,"none")
		});
	});
	$('nav a[href*=#]').click(function(event){
		event.preventDefault();
		var delta = 0;
		$('nav a').each(function () {
			$('nav a').removeClass('on');
		});
		$(this).addClass('on');

		var target = event.currentTarget.hash;
		$target = $(target);
		$('html, body')
			.stop()
			.animate({
				'scrollTop': $target.offset().top
			}, 500, 'swing', function () {
				window.location.hash = target;
				$(document).on("scroll", onScroll);
			}
		);
	});
	
	$('.right_bar span').click(function(){
		if($(".right_bar").hasClass("topbt")){
			console.log("top")
			$('html, body')
				.stop()
				.animate({
					'scrollTop': 0}, 500, 'swing', function () {
					$(document).on("scroll", onScroll);
				}
			);
		}
		
	});
});
//메뉴스크롤
function onScroll() {
	var _scrollPos = $(document).scrollTop()+76;
	//var start =$("#intro .headtit").position().top
	var currbox =$('nav a.on').attr("rel");
	
	$('nav a[href*=#]').each(function() {
		var currLink = $(this);
		var refElement = $(currLink.attr("href"));

		if ( refElement.position().top <= _scrollPos && refElement.position().top + refElement.height() > _scrollPos ) {
			$('.nav a').removeClass("on");
			currLink.addClass("on");
			console.log(currbox)
			$("#"+currbox).addClass("motion");
			if(currbox == "main"){
				
				$(".right_bar").removeClass("topbt").find("span").removeClass("top");
			}else{
				$(".right_bar").addClass("topbt").find("span").addClass("top");
			}
		} else {
			currLink.removeClass("on");

		}
	});
	
}
function snsShare(type) {
	var fn_url = "https://www.facebook.com/V8GHOSTHOUSE/";
	var fn_title = $('meta[property="og:title"]').attr('content');
	var fn_decr = $('meta[property="og:description"]').attr('content');
	var fn_img = $('meta[property="og:image"]').attr('content');
	if(type=="fb") window.open("http://www.facebook.com/sharer.php?u="+encodeURIComponent(fn_url) + "&t=" + encodeURIComponent(fn_title) +"&i"+encodeURIComponent(fn_img)+"&i"+encodeURIComponent(fn_decr),"","width = 530, height = 560, scrollbars = true");
 	
}
//popup
function lock_touch(e){
	e.preventDefault();
}
function popOpen(id){
	var _this = $('#' + id);
	_this.stop(true, false).fadeIn(150);
	_this.addClass('open');
	console.log(_this)
	
	var popCon = _this.find('.pop_layer');
	var winH = $(window).height();
	var popW = popCon.width();
	var popH = popCon.height();

	if(winH < popH){
		popCon.stop(true, false).css({'top': 50, 'marginTop': 0});
	} else {
		document.addEventListener('touchmove', lock_touch);
		popCon.stop(true, false).css({'marginTop': -(popH/2) + 50});
	}
	var popinChk = _this.hasClass('inpopup');
	if ( !popinChk ){
		$('html').addClass('pop_open');
		$('<span class="bg_pop"></span>').appendTo('body');
	}
}
function popClose(id){
	var _this = $('#' + id);
	_this.removeClass('open').hide();

	var popCon = _this.find('.pop_layer');
	popCon.css({'top': '50%', 'marginTop': '50px'});

	document.removeEventListener('touchmove', lock_touch);

	var popinChk = _this.hasClass('inpopup');
	if ( !popinChk ){
		$('.bg_pop').remove();
		$('html').removeClass('pop_open');
	}
}
var popResize = function(){
	if( $('.pop_layer').hasClass('open') ){
		var winH = $("#wrap").height();
		var popCon = $('.pop_layer.open').find('.pop_layer');
		var popConH = $('.pop_layer.open').find('.pop_layer').height() + 200;

		var popinChk = $('.pop_layer.open').hasClass('inpopup');
		if ( !popinChk ){
			if ( winH < popConH ){
				document.removeEventListener('touchmove', lock_touch);
				popCon.stop(true, false).css({'top': 50, 'marginTop': 0});
			} else {
				document.addEventListener('touchmove', lock_touch);
				popCon.stop(true, false).css({'top': '50%', 'marginTop': -(popConH/2) + 50});
			}
		}
	}
}







