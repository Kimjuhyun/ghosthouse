// pages
var express = require('express');
var router = express.Router();

// page connect
router.get('/', function(req, res) {
  
  if (req.query.r) {
    // req.session.inflowURL = hashids.decodeHex(req.query.r);
    req.session.inflowURL = req.query.r;
  } else {
    delete req.session.inflowURL;
  }
  var datakey = "";
  if(req.query.datakey)
      datakey = req.query.datakey
  if (req.is_mobile) {
      res.render('mobile_main', { title: 'Express' ,datakey : datakey});
  } else {
      res.render('main', { title: 'Express' ,datakey : datakey});
  }

  // var http = require('http');
  //
  // var options = {
  //     'host': 'api.instagram.com',
  //     'path': '/v1/tags/셀피/media/recent?access_token=5671054.81952b3.3148aaeeafcb4001bf34d212f9b544eb&scope=public_content'
  // };
  //
  // var req = http.request(options, function(res) {
  //     console.log('STATUS: ' + res.statusCode);
  //     console.log('HEADERS: ' + JSON.stringify(res.headers));
  //     res.setEncoding('utf8');
  //     res.on('data', function (chunk) {
  //         console.log('BODY: ' + chunk);
  //     });
  // });
  //
  // req.on('error', function(e) {
  //     console.log('problem with request: ' + e.message);
  // });
  //
  // // write data to request body
  // req.write('data\n');
  // req.write('data\n');
  // req.end();
});

router.get('/admin', function(req, res, next) {
  if (req.session.admin) {
    res.render('admin');
  } else {
    res.redirect('/admin/login');
  }
});

router.get('/admin/login', function(req, res, next) {
  if (req.session.admin) {
    res.redirect('/admin');
  } else {
    res.render('login');
  }
});

module.exports = router;
